<?php

//Класс инициализации
class Init {

    private function __construct() {}

    //Метод инициализации приложения
    public static function init() {
        //Получаем статус пользователя
        $login = Session::getInstance()->status;

        if($login) {
            //Если выполнен вход
            $db = Db::getInstance();

            //Получаем ID пользователя в БД
            $uid = (int)Session::getInstance()->uid;

            //Получаем круги текущего пользователя
            $sql = 'SELECT * FROM `user` AS c1 INNER JOIN `circle` AS c2 WHERE c1.`id` = c2.`user_id` AND `user_id` = '.$uid;
            $circles = $db->dbCon->query($sql)->fetchAll();

            //Подключаем главную страницу
            require_once './view/index.php';
        } else {
            //Если не выполнен вход то подключаем страницу входа
            require_once './view/login.php';
        }
    }

    //Метод подключения хедера
    public static function getHeader() {
        require_once './view/layout/header.php';
    }

    //Метод подключения футера
    public static function getFooter() {
        require_once './view/layout/footer.php';
    }
}

?>