<?php

//Класс для обработки сессии
class Session {
    protected static $_instance;

    public $status; // Статус входа
    public $uid; // ID пользователя в БД

    private function __construct() {
        try{
            session_start();

            if (!isset($_SESSION['login'])) {
                $this->status = false;
            } else {
                //Если выполнен вход
                $this->status = true;
                $this->uid = $_SESSION['uid'];
            }
        } catch (Exception $e) {
            //Вывод сообщения об ошибке
            echo $e->getMessage();
        }
    }

    public static function getInstance() {
        if (self::$_instance === null) {
            //Если текущего объекта нет то создаем
            self::$_instance = new self;
        }

        return self::$_instance;
    }

    //Метод меняет статус залогиненного пользователя
    public function changeStatus($login,$uid) {
        //Получаем текущий объект
        $sess = self::getInstance();

        //Пишем в сессию
        $_SESSION['login'] = $login;
        $_SESSION['uid'] = $uid;

        //Пишем в переменные объекта
        $sess->status = true;
        $sess->userId = $uid;

        return true;
    }

}

?>