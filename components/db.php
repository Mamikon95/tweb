<?php

// Класс для работы с базой данных
class Db {
    protected static $_instance;

    //В переменной хранится объект PDO
    public $dbCon;

    private function __construct() {
        try{
            //Создаем подключнение
            $this->dbCon = new PDO('mysql:host=localhost;dbname=tweb', 'root', '');
        } catch (PDOException $e) {
            //Если возникла ошибка то вывод сообщения об ошибке
            echo $e->getMessage();
        }
    }

    //Singleton подключение
    public static function getInstance() {
        if (self::$_instance === null) {
            //Если текущего объекта нет то создать
            self::$_instance = new self;
        }

        return self::$_instance;
    }



}

?>