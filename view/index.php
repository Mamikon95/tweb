<?php Init::getHeader() //вызов хедера?>
<div class="content">
    <div class="field">
        <?php foreach($circles as $circle):?>
            <div class="circle" style="
                width:<?=$circle['radius']*2?>px;
                height:<?=$circle['radius']*2?>px;
                background:<?=$circle['color']?>;
                left: <?=$circle['left']?>px;
                top: <?=$circle['top']?>px;
                "></div>
        <?php endforeach;?>
    </div>
    <div class="field-bt">
        <a href="javascript:void(0)" class="new-dot" id="new-dot">Новая точка</a>
        <a href="javascript:void(0)" class="dot-check" id="check">Проверить</a>
    </div>
</div>

<!-- Modal -->
<div id="check-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <button type="button" class="btn btn-danger" id="clear-circle">Очистить</button>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>

    </div>
</div>
<?php Init::getFooter() //вызов футера?>
