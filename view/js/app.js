
//Генерация круга - HTML
function generateCircle(left,top,radius,color) {
    var content = '<div class="circle" style="' +
        'width: ' + radius*2 + 'px;' +
        'height: ' + radius*2 + 'px;' +
        'background: ' + color + ';' +
        'left: ' + left + 'px;' +
        'top: ' + top + 'px;' +
        '"></div>';
    return content;
}


$(document).ready(function () {


    //Новый круг
    $('body').delegate('#new-dot','click',function (e) {
        //POST запрос на request.php
        $.post('/ajax/request.php',{new: true},function (data) {
            try
            {
                var res = JSON.parse(data);
                var circle_content = generateCircle(res.left,res.top,res.radius,res.color);
                $('.field').append(circle_content);

            }
            catch(e)
            {
                alert('Возникла ошибка');
            }
        })
    })

    //Проверка пересекающийхся кругов
    $('body').delegate('#check','click',function (e) {
        //POST запрос на request.php
        $.post('/ajax/request.php',{check: true},function (data) {
            $('#check-modal .modal-title').text('Пересечения у ' + data + ' кругов');
            if(data == 0) {
                $('#clear-circle').prop('disabled','true');
            } else {
                $('#clear-circle').removeAttr('disabled');
            }
            //Вызываем модальку
            $('#check-modal').modal('show');
        })
    })

    //Очистка кругов пользователя
    $('body').delegate('#clear-circle','click',function (e) {
        $(this).prop('disabled','true');
        //POST запрос на request.php
        $.post('/ajax/request.php',{clear: true},function (data) {
            $('#clear-circle').removeAttr('disabled');
            if(data != 'OK') {
                alert('Попробуйте еще раз!');
            } else {
                //Скрываем модальку
                $('#check-modal').modal('hide');
                $('.field').html('');
            }
        })
    })
})