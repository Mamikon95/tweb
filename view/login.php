<?php Init::getHeader()?>
<div class="panel panel-default">
    <div class="panel-heading">Войдите в систему</div>
    <div class="panel-body">
        <form method="post" action="/requests/login.php">
            <div class="form-group">
                <label for="user">Логин:</label>
                <input type="text" class="form-control" id="user" name="user" required>
            </div>
            <div class="form-group">
                <label for="pass">Пароль:</label>
                <input type="password" class="form-control" id="pass" name="pass" required>
            </div>
            <button type="submit" class="btn btn-default">Вход</button>
        </form>
    </div>
</div>

<?php Init::getFooter()?>
