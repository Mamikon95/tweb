<?php
//Подключаем компоненты
require_once '../components/db.php';
require_once '../components/user.php';
require_once '../components/session.php';


//Проверка отправленных данных
$username = isset($_POST['user']) ? $_POST['user'] : false;
$pass = isset($_POST['pass']) ? $_POST['pass'] : false;

//Если они есть
if($username && $pass) {

    //Создаем объект пользователя
    $user = new User();

    //Проверка залогинен ли пользователь?
    $login = $user->login($username,$pass);

    //Если залогинен
    if($login) {

        //Ставим сессию о том что пользователь залогинен
        Session::getInstance()->changeStatus(true,$login);
    }
}


header('Location: /');

?>