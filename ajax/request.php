<?php
//Если Ajax запрос
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

    //Подключаем компоненты
    require_once '../components/db.php';
    require_once '../components/session.php';
    require_once '../components/helper.php';

    //Если существует одна из данных переданных
    $new = isset($_POST['new']) ? $_POST['new'] : false;
    $check = isset($_POST['check']) ? $_POST['check'] : false;
    $clear = isset($_POST['clear']) ? $_POST['clear'] : false;

    //Получаем id пользователя из сессии
    $user_id = (int)Session::getInstance()->uid;

    if ($new) {
        //Если запрос о создании круга
        $radius = rand(10, 40);
        $top = rand(0, (500 - $radius * 2));
        $left = rand(0, (500 - $radius * 2));

        //Получаем цвет
        $color = Helper::getColor();

        // вставляем в таблицу
        $sql = "INSERT INTO `circle` (`left`, `top`, `radius`, `color`, `user_id`) VALUES (:left_pos, :top, :radius, :color, :user_id)";
        $insert_val = Db::getInstance()->dbCon->prepare($sql);
        $insert_val->bindValue(':left_pos',$left);
        $insert_val->bindValue(':top',$top);
        $insert_val->bindValue(':radius',$radius);
        $insert_val->bindValue(':color',$color);
        $insert_val->bindValue(':user_id',$user_id);
        $inserted = $insert_val->execute();

        //Если вставилось
        if($inserted) {
            //Массив для передачи JSON объекта
            $data = [
                'radius' => $radius,
                'top' => $top,
                'left' => $left,
                'color' => $color
            ];

            // Передаем JSON объект
            echo json_encode($data);
        }
    } else if($check) {
        //Если запрос на проверку кол-ва пересекающийся кругов
        $sql = 'SELECT COUNT(DISTINCT(c1.`id`)) as `cnt` FROM `circle` AS c1 
                INNER JOIN `circle` AS c2 ON c1.`user_id` = c2.`user_id` 
                WHERE SQRT(POW(((c1.`top` + c1.`radius`) - (c2.`top` + c2.`radius`)),2) + POW(((c1.`left` + c1.`radius`) - (c2.`left` + c2.`radius`)),2)) <= (c1.`radius` + c2.`radius`) AND c1.`id` <> c2.`id` AND c1.`user_id` = '.$user_id;
        $res = Db::getInstance()->dbCon->query($sql)->fetchColumn();

        //Передаем кол-во кругов
        echo $res;

    } else if($clear) {
        //Удаляем все круги пользователя
        $sql = 'DELETE FROM `circle` WHERE `user_id` = '.$user_id;
        $res = Db::getInstance()->dbCon->query($sql)->fetchColumn();
        echo 'OK';
    }
} else {
    //Редирект на главную страницу
    header('Location: /');
}
?>